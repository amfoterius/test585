<style>
    .myForm {
        width: 800px;
        margin: 60px auto;
    }

    .myForm * {
        margin-top: 20px;
    }
</style>


{!! Form::open(['url' => 'store', 'method' => 'post', 'class' => 'myForm']) !!}

    @if ($errors->any())
        <div style="color: red">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{ \App\Libraries\FormGeneration::generateForRules($validation_rules) }}

<input type="submit" value="OK">
{!! Form::close() !!}

<?php
/**
 * Created by PhpStorm.
 * User: magomed
 * Date: 26.02.2018
 * Time: 16:47
 */

namespace App\Libraries;

use Validator;
use Form;

class FormGeneration
{

    /**
     *
     * @var array
     */
    public static $types_input = [
        'date',
        'file',
        'numeric',
        'email'
    ];


    /**
     * Формирует поля для ввода, на основе правил валидации
     *
     * @param array $all_rules
     */
    public static function generateForRules(array $all_rules)
    {
        foreach($all_rules as $input_name => $rules) {

            $type_input = 'text';
            $attributes = [];

            $rules = explode("|", $rules);

            // Определяем type input
            foreach(self::$types_input as $row) {
                if(($i = array_search($row, $rules)) !== false) {
                    $type_input = $rules[$i];
                    unset($rules[$i]);
                }
            }


            foreach($rules as $rule) {

                $arr_rule = explode(":", $rule);

                switch($arr_rule[0]) {

                    // Можно в дальнейшем добавлять другие проверки, через атрибуты делать проверку на стороне фронта
                    case "min":
                        if(in_array($type_input, ['text', 'email'])) {
                            $attributes['minlength'] = $arr_rule[1];

                        } else {
                            $attributes[$arr_rule[0]] = $arr_rule[1];
                        }
                        break;

                    case "max":
                        if(in_array($type_input, ['text', 'email'])) {
                            $attributes['maxlength'] = $arr_rule[1];
                        } else {
                            $attributes[$arr_rule[0]] = $arr_rule[1];
                        }
                        break;


                    // По умолчанию вешаем аттрибут соотв правилу
                    default:
                        if(count($arr_rule) === 1) {
                            $attributes[$arr_rule[0]] = $arr_rule[0];
                        } elseif (count($arr_rule) === 2) {
                            $attributes[$arr_rule[0]] = $arr_rule[1];
                        } else {
                            throw new Exception('Error validation rule: '.$rule);
                        }

                        break;

                }


            }

            echo "<div>";
            // Исключение
            if($type_input == "numeric") {
                echo Form::number($input_name, '', $attributes);
            } else {
                echo Form::$type_input($input_name, '', $attributes);
            }

            echo "</div>";

        }


    }


}
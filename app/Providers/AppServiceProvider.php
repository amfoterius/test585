<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        // Валидация для фибаначи
        // TODO: Вынести в отдельный класс
        Validator::extend('onFib45', function ($attribute, $value, $parameters, $validator) {

            $value = (int)$value;

            if($value === 1) return true;

            $fib_1 = 1;
            $fib_2 = 1;

            // Можно просто закешировать последовательность, но я оставлю это здесь
            for($i = 0; $i < 45; $i++) {
                $fib_summ = $fib_1 + $fib_2;

                if($value < $fib_summ) {
                    return false;

                } elseif ($value === $fib_summ) {
                    return true;
                }

                $fib_1 = $fib_2;
                $fib_2 = $fib_summ;
            }

            return false;

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

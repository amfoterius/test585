<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{

    public static $rules = [
        'title' => 'required|max:255',
        'mmm' => 'required|email',
        'regex_field' => 'regex:/[a-zA-Z]+/',
        'fib' => "numeric|onFib45"
    ];

    public function __construct(Request $request)
    {
        $this->data['validation_rules'] = self::$rules;
    }


    public function index(Request $request)
    {
        return view('base', $this->data);
    }

    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(), $this->data['validation_rules']);

        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }


        return view('result', $this->data);

    }

}

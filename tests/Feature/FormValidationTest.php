<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FormValidationTest extends TestCase
{

    private $variants = [];

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        foreach(\App\Http\Controllers\BaseController::$rules as $input_name => $rules) {


            $type_input = 'text';
            $rules = explode("|", $rules);

            foreach(\App\Libraries\FormGeneration::$types_input as $row) {
                if(($i = array_search($row, $rules)) !== false) {
                    $type_input = $rules[$i];
                }
            }

            switch($type_input) {

                case 'text':
                    $this->variants[$input_name] = $this->setDataForText($rules);
                    break;

                case 'email':
                    $this->variants[$input_name] = $this->setDataForText($rules);
                    break;

                default:
                    break;
            }

        }

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $t = $this->get('/');

        $result = true;
        foreach(\App\Http\Controllers\BaseController::$rules as $input_name => $rules) {
            $rand_index = rand(0, count($this->variants[$input_name]));

            $result = $result && $this->variants[$input_name][$rand_index][0];
            $t = $t->type($this->variants[$input_name][$rand_index][1], $input_name);

        }

        if($result) {
            $t->seePageIs('/store');
        } else {
            $t->seePageIs('/');
        }
    }


    /**
     *
     * Формирует тестовые данные, для строковых полей ввода
     *
     * @param array $rules
     * @param int $count Количество вариантов
     * @param int $min Минимальная длина слова
     * @param int $max Максимальная длина слова
     * @return array
     */
    private function setDataForText(array $rules, $count = 10, $min=0, $max=200) {

        $result = [];


        for($i = 0; $i <= $count; $i++) {

            $str = str_random(random_int($min, $max));

            $current = true;
            foreach($rules as $rule) {
                $rule_arr = explode(":", $rule);

                if($rule_arr[0] === "min") {
                    if(count($str) < (int)$rule_arr[1]) $current = false;
                    break;
                } elseif ($rule_arr[0] === "max") {
                    if(count($str) > (int)$rule_arr[1]) $current = false;
                    break;
                } elseif ($rule_arr[0] === "required") {
                    if(count($str) === 0) $current = false;
                    break;
                } elseif ($rule_arr[0] === "regex") {
                    if(!preg_match($rule_arr[1], $str)) $current = false;
                    break;
                }

            }

            $result[] = [$current, $str];

        }

        return $result;

    }


    /**
     *
     * @param array $rules
     * @param int $count
     * @param int $min Минимальная длина логина
     * @param int $max Максимальная длина логина
     * @return array
     */
    private function setDataForEmail(array $rules, $count = 10, $min=0, $max=20) {

        $result = [];

        for($i = 0; $i <= $count/2; $i++) {
            $str = str_random(random_int($min, $max)).'@'.str_random(random_int(2, 20)).'.'.str_random(random_int(2, 4));

            $current = true;
            foreach($rules as $rule) {
                $rule_arr = explode(":", $rule);

                if($rule_arr[0] === "min") {
                    if(count($str) < (int)$rule_arr[1]) $current = false;
                    break;
                } elseif ($rule_arr[0] === "max") {
                    if(count($str) > (int)$rule_arr[1]) $current = false;
                    break;
                } elseif ($rule_arr[0] === "required") {
                    if(count($str) === 0) $current = false;
                    break;
                } elseif ($rule_arr[0] === "regex") {
                    if(!preg_match($rule_arr[1], $str)) $current = false;
                    break;
                }

            }

            $result[] = [$current, $str];

        }

        for($i = 0; $i <= ($count - $count % 2)/2; $i++) {
            $str = str_random(random_int($min, $max));

            $result[] = [false, $str];

        }

        return $result;
    }

    // TODO: Сделать генерацию для чисел
    private function setDataForNumeric(array $rules) {

    }

}
